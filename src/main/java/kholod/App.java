package kholod;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);

        BMWDealership bmwDealership = ctx.getBean(BMWDealership.class);
        bmwDealership.getBmwx3().move();
        bmwDealership.getBmwx5().move();
    }
}
