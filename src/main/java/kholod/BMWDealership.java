package kholod;

public class BMWDealership {
    private BMW bmwx3;
    private BMW bmwx5;

    public BMWDealership(BMW bmwx3, BMW bmwx5) {
        this.bmwx3 = bmwx3;
        this.bmwx5 = bmwx5;
    }

    public BMW getBmwx3() {
        return bmwx3;
    }

    public BMW getBmwx5() {
        return bmwx5;
    }
}
