package kholod;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean("bmwx3")
    public BMW bmwx3() {
        return new BMWX3();
    }

    @Bean("bmwx5")
    public BMW bmwx5() {
        return new BMWX5();
    }

    @Bean
    public BMWDealership bmwDealership(@Qualifier("bmwx3") BMW bmwx3, @Qualifier("bmwx5") BMW bmwx5) {
        return new BMWDealership(bmwx3, bmwx5);
    }
}
