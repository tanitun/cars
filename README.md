# Cars#

## Task ##

Implement class BMWDealership using Spring and annotation @Qualifier.

## Build and Run ##

### Prerequisites ###

- java 8+
- Maven 3.5

### Build project ###

- Clone repository
- Go to the folder
- `mvn clean package`

### Run ###

- `java -jar cars-1.0-SNAPSHOT-jar-with-dependencies.jar`